import os
import time

lines_count = 0
by_lng = {}
by_part = {}

lngs = ('py', 'cpp', 'h', 'hpp')
parts = ('class ', 'enum ')

for lng in lngs:
    by_lng[lng] = 0
for part in parts:
    by_part[part] = 0

start_time = time.time()
for top, dirs, files in os.walk(r'C:\code\trunc'):
    if top.find('.git') == -1:
        #print(lines_count, top)
        for nm in files:
            file = os.path.join(top, nm)
            try:
                if nm.split('.')[1] in lngs:
                    #print(nm)                     
                    lines_count += sum(1 for l in open(file, 'r'))
                    for part in parts:
                        for l in open(file, 'r'):
                            if l.find(part) > -1:
                                by_part[part] += 1
                    pass
                for lng in lngs:
                    if nm.split('.')[1] == lng:
                        by_lng[lng] += sum(1 for l in open(file, 'r'))
            except IndexError as e:
                pass
            except Exception as e:
                print(e, file)

print('script works ', time.time() - start_time)
print('=== ', lines_count, ' ===')
for lng in lngs:
    print(lng, by_lng[lng])
print('count of words')
for part in parts:
    print(part, by_part[part])
