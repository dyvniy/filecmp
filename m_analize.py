﻿import sqlite3
import os

def ana():
    conn = sqlite3.connect("filesC.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    req = '''
SELECT filename, st_size, count(st_size), 
(COUNT(st_size)-1)*st_size/1024/1024 as "sum Mb", 
GROUP_CONCAT(st_size), GROUP_CONCAT(dp), GROUP_CONCAT(st_mtime) 
FROM files
WHERE st_size > 1024*1024
GROUP BY filename 
HAVING COUNT(st_size) > 2 
ORDER BY COUNT(st_size)*st_size DESC
    '''
    req = """SELECT st_size, GROUP_CONCAT(filename), count(st_size) as sizes, st_size * count(st_size) as st_sum,
/*GROUP_CONCAT(st_size),*/ sum(st_size) as sum_size, dp, GROUP_CONCAT(dp), GROUP_CONCAT(st_mtime) 
FROM files
WHERE st_size > 1024*1024
GROUP BY st_size 
HAVING COUNT(st_size) > 2 AND COUNT(st_size) < 100
ORDER BY sum_size DESC
    """
    ans = conn.execute(req)
    stop = False
    count = 0
    for rec in ans:
        #print(rec)
        sizes = rec[4].split(',')
        paths = rec[5].split(',')
        times = rec[6].split(',')
        filename = rec[0]
        for i in range(0, len(sizes)-1):
            for j in range(i+1, len(sizes)-1):
                if (sizes[i] == sizes[j] and
                        abs(float(times[i]) - float(times[j])) < 10):
                    d1 = paths[i][:3]
                    d2 = paths[j][:3]
                    try:
                        if d1 == "D:/" and d2 == "E:/":
                            fr = os.path.join(paths[j], filename)
                            #os.remove(fr)
                            print(fr, ' removed')
                        if d2 == "D:/" and d1 == "E:/":
                            fr = os.path.join(paths[i], filename)
                            #os.remove(fr)
                            print(fr, ' removed')
                        if os.path.isfile(os.path.join(paths[i], filename)):
                            print(sizes[i], rec[0], '\n', paths[i], '\n', paths[j],
                                '\n', times[i], times[j], '\n')
                            count += 1
                            #input()
                    except FileNotFoundError:
                        pass
                    except PermissionError:
                        print("permission ", filename)
                    #input()
                    if (count % 10 == 0):
                        #stop = True
                        #break
                        #input()
                        pass
        if stop:
            break

if __name__ == "__main__":
    ana()
