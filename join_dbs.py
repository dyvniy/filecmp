import sqlite3
import os
from time import time

def joindb():
    jconn = sqlite3.connect("joined.db")
    jcursor = jconn.cursor()

    # Создание таблицы
    try:
        pass
        jcursor.execute('DROP TABLE files')
        jcursor.execute('DROP TABLE folders')
    except:
        print("tables not exist yet!")

    jcursor.execute("""CREATE TABLE files
(id int, st_mode int, st_ino int, st_dev int, st_nlink int, st_uid int,
st_gid int, st_size int, st_atime int, st_mtime int,
st_ctime int, dp text, filename text)""")
    jcursor.execute("""CREATE TABLE folders (id int, st_size int, dp text)""")

    dblist = ('filesD.db', 'filesE.db', 'filesG.db', 'filesH.db', )
    i = 0
    for db in dblist:
        conn = sqlite3.connect(db) # или :memory: чтобы сохранить в RAM
        cursor = conn.cursor()
        ans = cursor.execute('select * from files')
        print(db, i)
        for rec in ans:
            i += 1
            s = """INSERT INTO files VALUES ({}, {},{},{},{},{},{}, {},{},{}, {},"{}","{}")""".format(
                    i, *rec[1:])
            jcursor.execute(s)
            if i%100000 == 0:
                print(i)
        jconn.commit()

if __name__ == "__main__":
    beg = time()
    joindb()
    print('dbs joined! ' + str(time() - beg))
