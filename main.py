﻿import os
import sqlite3
import time

mypath = r"C:"

def main(path0, db0, i):
    conn = sqlite3.connect(db0) # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    # Создание таблицы
    try:
        pass
        #cursor.execute('DROP TABLE files')
        #cursor.execute('DROP TABLE folders')
    except:
        print("tables not exist yet!")

    cursor.execute("""CREATE TABLE files
                  (id int, st_mode int, st_ino int, st_dev int, st_nlink int, st_uid int,
                  st_gid int, st_size int, st_atime int, st_mtime int,
                  st_ctime int, dp text, filename text)""")
    cursor.execute("""CREATE TABLE folders
                  id int, st_size int, dp text""")
    
    stop = False
    for dirpath, dirnames, filenames in os.walk(path0):
        #print(dirpath, dirnames, filenames)
        #break
        if stop:
            break
        for f in filenames:
            #if os.path.splitext(f)[1] == '.txt':
            namepath = os.path.join(dirpath, f) #"{}/{}".format(dp, f)
            try:
                st = os.stat(namepath)
            except:
                print("Not Found: ", namepath)
                continue
            #print(i, f, st)
            s = """INSERT INTO files VALUES ({}, {},{},{},{},{},{}, {},{},{}, {},"{}","{}")""".format(
                    i, st.st_mode, st.st_ino, st.st_dev, st.st_nlink, st.st_uid,
                    st.st_gid, st.st_size, st.st_atime, st.st_mtime, st.st_ctime,
                    dirpath, f)
            #print(s)
            try:
                cursor.execute(s)
            except Exception as e:
                print("error insert:\n", s, "\n")
                print(e)
                stop = True
                break
            #print()
            i += 1
            if i % 10000 == 0:
                conn.commit()
                print(i, time.ctime(), f, st)
                stop = True
    conn.commit()
    return i

def check():
    conn = sqlite3.connect("files.db") # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()
    data = cursor.execute("SELECT * FROM files")
    
def get_drives():
    drives = []
    bitmask = windll.kernel32.GetLogicalDrives()
    for letter in 'ABCDEFGHKLMNOPRST':
        if bitmask & 1:
            drives.append(letter)
        bitmask >>= 1
    return drives

def save_drives():
    drives = get_drives()
    for drive in drives:
        i = main(drive + ":/", "files" + drive + ".db", 0)
        print(i)

if __name__ == "__main__":
    iC = main(mypath, "files.db", 0); print(iC)
    # save_drives()
    # check()
    pass
